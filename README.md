# Version 1.0.0 Working Version (3/29/2016) #

This App gets a list of Sales Orders from the Data Warehouse where the Sales Order's DOS is 2 days in advance.  Once this list is gathered, it will check to see if the primary insurance is Medicare (and will check more insurances in the future) using the Brightree API and if it is, use the Eligible API to see what version of Medicare the patient has.  If the found Insurance does not match the Primary Insurance of the Patient, update the patient's Primary Insurance with the Found insurance.  
For example, if the Primary is Medicare, and we find out though the Eligible API that the patient has Bravo, a variant of Medicare, replace the primary Insurance of the patient with Bravo and remove Medicare from the Patient's insurances.

# To do List #
- Add Payors other than PA Medicare.
- More tests/checks per Trello Boards/Cards
- ~~Test on Live database with live values~~
- ~~Write TO brightree, rather than get data from Brightree Servers~~
- ~~Possible UI of some kind to more easily track errors or know if it is "working?" -- or more than a console? (Most likely not)~~
- ~~Does not write to the User 1 Field when it finds a Medicare Advantage Plan for some reason..~~

# Updates #
_____
- Now uses Data Warehouse to get Sales Order Info
_____
- Can now effectively loop through and search every patient in the List of Orders
_____
- Took away unnecessary tests to ensure I have accurate information/correct Sales Order
_____
- All Orders now are automatically Medicare Primary Payor and have a DOS in two days.