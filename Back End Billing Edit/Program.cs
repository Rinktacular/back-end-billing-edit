﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Globalization;

namespace Back_End_Billing_Edit
{
    class Program
    {
        static void Main(string[] args)
        {

            float count = 0;
        start:           

            //Get the Sales Orders.
            _2___SOSearches.SoGrabAllOrders orderSearch = new _2___SOSearches.SoGrabAllOrders();
            orderSearch.exceuteSalesOrderSearch();

           
            //If no Orders get returned
            if (orderSearch.selectedOrders.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("No Orders Found, Sleeping for 5 Minutes -- Never Found Any Orders");
                Thread.Sleep(TimeSpan.FromMinutes(5));
                goto start;
            }
            int n = -1;

       editStart:
            try
            {                
                Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "";
                Back_End_Billing_Edit.Initializations.PublicVars.isEligible = true;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Last Run " + DateTime.Now);
                Console.ForegroundColor = ConsoleColor.Yellow;
                float percent = count / orderSearch.selectedOrders.Count;
                Console.WriteLine(percent.ToString("P", CultureInfo.InvariantCulture) + " Complete (" + count + "/" + orderSearch.selectedOrders.Count + ")");
                count++;                             
                Console.ForegroundColor = ConsoleColor.White;
                n++;

                //start over if no orders remain in the order list
                if (n >= orderSearch.selectedOrders.Count)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("No Orders Found, Sleeping for 5 Minutes -- Finished All Orders In Current List.");
                    Thread.Sleep(TimeSpan.FromMinutes(5));
                    goto start;
                }

                //Pulls all of the Sales Order Payor Info
                Console.WriteLine("Pulling Sales Order Payor Info -- " + orderSearch.selectedOrders[n]);
                _2___SOSearches.SOPayorSearch payorSearch = new _2___SOSearches.SOPayorSearch();
                payorSearch.executeSOPayorSearchMethod(orderSearch.selectedOrders[n]);

                //Pulls all Sales Order Info
                Console.WriteLine("Pulling Sales Order Info -- " + orderSearch.selectedOrders[n]);
                _2___SOSearches.SOFetch soFetch = new _2___SOSearches.SOFetch();
                soFetch.executeSoFetchMethod(orderSearch.selectedOrders[n]);

                //Pulls all Patient Info
                Console.WriteLine("Pulling Patient Info -- " + orderSearch.selectedPatients[n]);
                _2___SOSearches.PTFetch ptFetch = new _2___SOSearches.PTFetch();
                ptFetch.executePTFetchMethod(orderSearch.selectedPatients[n]);

                //Pulls all Patient Payor Info
                Console.WriteLine("Pulling Patient Payor Info -- " + orderSearch.selectedPatients[n]);
                _2___SOSearches.PTPrimarySearch ptPrimary = new _2___SOSearches.PTPrimarySearch();
                ptPrimary.executePrimaryPayorSearchMethod(orderSearch.selectedPatients[n]);     
                               
                Console.WriteLine("Begining Eligible API Calls");
                string DOS = DateTime.Today.Date.ToString();
                string lastName = _2___SOSearches.PTPrimarySearch.ptPrimaryPayorInfoAll.Insured.Name.Last;
                string firstName = _2___SOSearches.PTPrimarySearch.ptPrimaryPayorInfoAll.Insured.Name.First;
                string ptPolicyNum = _2___SOSearches.PTPrimarySearch.ptPrimaryPayorInfoAll.Policy.PolicyNumber;
                string ptDOB = _2___SOSearches.PTPrimarySearch.ptPrimaryPayorInfoAll.Insured.BirthDate.ToString();

                //EligibleAPI + Brightree API
                _3___Eligibility_Searches.Medicare medicareCheck = new _3___Eligibility_Searches.Medicare();
                Console.WriteLine("Sending API Request...");
                Tuple<bool, string> apiCall = medicareCheck.apiMedicareVerify(firstName, lastName, ptPolicyNum, ptDOB, DOS);                    
                if(apiCall.Item1 == true)
                {
                    MessageBox.Show("We did it : " + apiCall.Item2);
                    soService.DataFetchServiceResponseUsingSalesOrder updateOrder = Initializations.Clients.soAction.SalesOrderFetchByBrightreeID(orderSearch.selectedOrders[n].ToString());
                    updateOrder.Items[0].SalesOrderGeneralInfo.StopDate = DateTime.Today.Date;
                    updateOrder.Items[0].SalesOrderGeneralInfo.User1 = apiCall.Item2;                

                    soService.DataWriteServiceResponse updateOrderResponse = Initializations.Clients.soAction.SalesOrderUpdate(Convert.ToInt32(updateOrder.Items[0].BrightreeID), updateOrder.Items[0]);
                    if(updateOrderResponse.Success == true)
                    {
                        MessageBox.Show("We did it : " + apiCall.Item2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(apiCall.Item2 + " added to UserBox1");
                        Console.WriteLine("Order # " + orderSearch.selectedOrders[n] + ", corrected for patientKey #" + orderSearch.selectedPatients[n]);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("-------------------------------------------------------------|");
                    }
                    else
                    {
                        MessageBox.Show("We Didnt do it");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Update Order Response Error Messages: ");
                        Console.WriteLine(updateOrderResponse.Messages);                            
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("-------------------------------------------------------------|");
                    }
                }
                else if(apiCall.Item2 == "Patient is Fine" && apiCall.Item1 == false)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(apiCall.Item2);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("---------------------------------------------------------|");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(apiCall.Item2);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("---------------------------------------------------------|");
                }            
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                goto editStart;
                
            }
       goto editStart;
       }

    }
}
