﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_End_Billing_Edit._2___SOSearches
{
    class SOFetch
    {
        public static soService.SalesOrder soAllInfo { get; set; }

        public void executeSoFetchMethod(int BrightreeSOID)
        {
            soService.DataFetchServiceResponseUsingSalesOrder soFetch = Initializations.Clients.soAction.SalesOrderFetchByBrightreeID(BrightreeSOID.ToString());
            foreach(soService.SalesOrder soResp in soFetch.Items)
            {
                soAllInfo = soResp;
            }
        }
    }
}
