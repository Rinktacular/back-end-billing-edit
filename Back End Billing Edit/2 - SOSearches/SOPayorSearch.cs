﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_End_Billing_Edit._2___SOSearches
{
    class SOPayorSearch
    {
        public static soService.SalesOrderPayorSearchResponse soPayorPrimary { get; set; }
        public void executeSOPayorSearchMethod(int BrightreeOrder)
        {
            var soPayorSearch = new soService.SalesOrderPayorSearchRequest();

            //set search params
            soPayorSearch.SOKey = new soService.LookupValue().ID;
            soPayorSearch.SOKey = BrightreeOrder;
            soPayorSearch.PayorLevelKey = new soService.LookupValue().ID;
            soPayorSearch.PayorLevelKey = 1; // <--- get the primary payor

            //set sort params
            var payorSortParam = new List<soService.SalesOrderPayorSortParameter>();
            var payorSorter = new soService.SalesOrderPayorSortParameter();
            payorSorter.SortField = soService.SalesOrderPayorSortField.PayorLvlKey;
            payorSorter.SortOrder = soService.SortOrder.Ascending;

            var soPayorSearcher = Initializations.Clients.soAction.SalesOrderPayorSearch(soPayorSearch, payorSortParam.ToArray(), 1, 1);

            //Evaluation of search
            foreach(soService.SalesOrderPayorSearchResponse soPayorSearchResponse in soPayorSearcher.Items)
            {
                soPayorPrimary = soPayorSearchResponse;
                //data goes to validation checks -- Program.cs
            }
        }
    }
}
