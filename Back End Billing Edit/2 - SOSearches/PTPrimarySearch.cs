﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_End_Billing_Edit._2___SOSearches
{
    class PTPrimarySearch
    {
        public static ptService.PatientPayor ptPrimaryPayorInfoAll { get; set; }

        public void executePrimaryPayorSearchMethod(int PatientBrightreeID)
        {
            //Get Payor and fetch Patient's Policy Number for primary insurance that matches a medicare payor
           ptService.DataFetchServiceResponseUsingPatientPayor ptPayorFetch = Initializations.Clients.ptAction.PatientPayorFetchAll(PatientBrightreeID);
           foreach(ptService.PatientPayor ptPayor in ptPayorFetch.Items)
           {
               if(ptPayor.payorLevel == ptService.PayorLevel.Primary && ptPayor.PayorKey == 103)
               {
                   ptPrimaryPayorInfoAll = ptPayor;
                   break;
               }                    
           }            
        }
    }
}
