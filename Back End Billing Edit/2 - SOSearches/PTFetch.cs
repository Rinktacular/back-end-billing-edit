﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_End_Billing_Edit._2___SOSearches
{
    class PTFetch
    {
        public ptService.Patient ptInfoAll { get; set; }

        public void executePTFetchMethod(int BrightreePTID)
        {
            ptService.DataFetchServiceResponseUsingPatient ptFetch = Initializations.Clients.ptAction.PatientFetchByBrightreeID(BrightreePTID.ToString());

            foreach(ptService.Patient patient in ptFetch.Items)
            {
                ptInfoAll = patient;
            }
        }
    }
}
