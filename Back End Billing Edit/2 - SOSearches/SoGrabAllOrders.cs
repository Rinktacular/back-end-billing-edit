﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SQL = System.Data;
using System.Windows.Forms;

namespace Back_End_Billing_Edit._2___SOSearches
{
    class SoGrabAllOrders
    {
        public List<int> selectedOrders { get; set; }
        public List<int> selectedPatients { get; set; }

        SqlConnection cnn;
        public void exceuteSalesOrderSearch()
        {
            selectedOrders = new List<int>();
            selectedPatients = new List<int>();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Opening SQL Connection to [Brightree].[dbo].[Sales Order]...");

            string connectionstring = "104.209.184.65";

            connectionstring = "Data Source=104.209.184.65;Initial Catalog=BrightreeData;Persist Security Info=True;User ID=Reporting;Password=Qmes2015";
            cnn = new SqlConnection(connectionstring);
            cnn.Open();
            Console.WriteLine("Sending SQL Request");

            string sql = "SELECT [BrightreeData].[dbo].[SalesOrder].[SOKey], [BrightreeData].[dbo].[SalesOrder].[PtKey], p.PayorName FROM [BrightreeData].[dbo].[SalesOrder] INNER JOIN [BrightreeData].[dbo].[SalesOrderPayor] sop ON [BrightreeData].[dbo].[SalesOrder].SOKey = sop.SOKey AND [BrightreeData].[dbo].[SalesOrder].NickName = sop.NickName AND sop.PayorLevel = 'Primary' INNER JOIN BrightreeData.dbo.Payor p ON p.NickName = sop.NickName  AND p.PayorKey = sop.PayorKey WHERE [BrightreeData].[dbo].[SalesOrder].[Nickname] = 'StlukesHS' AND [BrightreeData].[dbo].[SalesOrder].[SONextDOSDT] = " +"'" + DateTime.Today.Date.AddDays(2).ToString().Substring(0,9) + "'" + "AND [BrightreeData].[dbo].[SalesOrder].[SOStatus] IN ('Active', 'On-Hold') AND sop.[PayorKey] = '103' AND sop.[PayorLevel] = 'Primary'";
            
            Console.WriteLine("Creating SQL Table");
            SQL.DataTable dtable = new SQL.DataTable();

            SqlDataAdapter dscmd = new SqlDataAdapter(sql, cnn);
            SQL.DataSet ds = new SQL.DataSet();
            try
            {
                dscmd.Fill(dtable);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            Console.WriteLine("Writing [SOKey], [PtKey] Lists");
            string[] list = new string[dtable.Rows.Count];
            for (int i = 0; i < dtable.Rows.Count; i++)
            {
                selectedOrders.Add(Convert.ToInt32(dtable.Rows[i]["SOKey"]));
                selectedPatients.Add(Convert.ToInt32(dtable.Rows[i]["PtKey"]));
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Sales Order: " + dtable.Rows[i]["SOKey"]);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Patient Key: " + dtable.Rows[i]["PtKey"]);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(dtable.Rows.Count - i + " Items remaining");
                
            }
            Console.WriteLine("Lists Complete, Starting Edits..");
            
        }
    }
}
