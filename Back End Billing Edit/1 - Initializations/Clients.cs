﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Back_End_Billing_Edit.Initializations
{
    class Clients
    {
        public static soService.SalesOrderServiceClient soAction = createSalesOrderServiceClient();
        public static ptService.PatientServiceClient ptAction = createPatientServiceClient();
        public static insService.InsuranceServiceClient insAction = createInsuranceServiceClient();

        private static insService.InsuranceServiceClient createInsuranceServiceClient()
        {

            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1210/OrderEntryService/InsuranceService.svc");
            insService.InsuranceServiceClient client = new insService.InsuranceServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.userName;
            client.ClientCredentials.UserName.Password = PublicVars.password;

            return client;
        }

        private static ptService.PatientServiceClient createPatientServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1302/OrderEntryService/patientservice.svc");
            ptService.PatientServiceClient client = new ptService.PatientServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.userName;
            client.ClientCredentials.UserName.Password = PublicVars.password;

            return client;
        }

        public static soService.SalesOrderServiceClient createSalesOrderServiceClient()
        {
            BasicHttpBinding myBinding = new BasicHttpBinding();
            myBinding.MaxReceivedMessageSize = 65536 * 2;
            myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            EndpointAddress ea = new EndpointAddress("https://webservices.brightree.net/v0100-1508/OrderEntryService/SalesOrderService.svc");
            soService.SalesOrderServiceClient client = new soService.SalesOrderServiceClient();
            client.ClientCredentials.UserName.UserName = PublicVars.userName;
            client.ClientCredentials.UserName.Password = PublicVars.password;

            return client;
        }
       
    }
}
