﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using OopFactory.X12.Hipaa.Eligibility;
using OopFactory.X12.Hipaa.Eligibility.Services;
using System.IO;
using System.Windows.Forms;

namespace Back_End_Billing_Edit._3___Eligibility_Searches
{
    class Medicare
    {
        public Tuple<bool,string> apiMedicareVerify(string ptFirstNameData, string ptLastNameData, string ptPolicyNum, string ptDOBData, string DOSData)
        {
            //variables
            //string apiSecret = "rM9wNT9y8ffeBAGl9s_eqyJ5G4oX2CgEVrkn";
            string apiSecret = "yPwIRLK2rCvCU3LAVl-4n8cOQtr3A4RbLssE";
            string payerID = "0431";
            string providerOrgName = "Montgomery Medical Equipment Company";
            string ptPolicyID = ptPolicyNum;
            string ptFirstName = ptFirstNameData;
            string ptLastName = ptLastNameData;
            string ptDOB = Convert.ToDateTime(ptDOBData).ToString("yyyy-MM-dd");
            string DOS = Convert.ToDateTime(DOSData).ToString("yyyy-MM-dd");
            string serviceType = "18";
            string NPI = "1134303902";

            #region DME Eligibility
            RestClient client = new RestClient("https://gds.eligibleapi.com/v1.5/coverage/all?api_key=" + apiSecret + "&payer_id=" + payerID + "&service_provider_organization_name=" + providerOrgName + "&provider_npi=" + NPI + "&member_id=" + ptPolicyID + "&member_first_name=" + ptFirstName
                + "&member_last_name=" + ptLastName + "&member_dob=" + ptDOB + "&date=" + DOS + "&service_type=" + serviceType + "&format=x12");

            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            string response271 = response.Content;
            Stream response271Stream = Back_End_Billing_Edit.Initializations.StringStream.GenerateStreamFromString(response271);

            var service = new EligibilityTransformationService();
            EligibilityBenefitDocument eligibilityBenefitDocument = service.Transform271ToBenefitResponse(response271Stream);
            eligibilityBenefitDocument.EligibilityBenefitResponses = eligibilityBenefitDocument.EligibilityBenefitResponses;

            if (eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos.Count < 1)
            {
                Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "Subscriber Info Invalid";
                return Tuple.Create(false, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);
            }

            //checks for Medicare secondary
            foreach (EligibilityBenefitInformation benefitInfo in eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos)
            {
                //If the benefit info contains a date range
                if (benefitInfo.DateRanges.Count > 0)
                {
                    //If the benefit info relates to Medicare Part A and the date range either ends before December 31st, or starts after January 1st..
                    if (benefitInfo.InsuranceType.Code == "MA" && (benefitInfo.DateRanges[0].EndDate.DayOfYear < 365 || benefitInfo.DateRanges[0].BeginDate.DayOfYear > 1))
                    {
                        //if the DOS is after when the patient's benefit info began and DOS is prior to the end of the benefit info.
                        if (Convert.ToDateTime(DOS) > benefitInfo.DateRanges[0].BeginDate & Convert.ToDateTime(DOS) < benefitInfo.DateRanges[0].EndDate)
                        {
                            //Write back saying "Skilled - start date - end date" to user 1 field
                            Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "Skilled - " + benefitInfo.DateRanges[0].BeginDate.ToShortDateString() + " - " + benefitInfo.DateRanges[0].EndDate.ToShortDateString();
                            return Tuple.Create(false, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);
                        }
                    }
                }
            }

            string benefitCodes = "";
            foreach (EligibilityBenefitInformation benefitInfo in eligibilityBenefitDocument.EligibilityBenefitResponses[0].BenefitInfos)
            {
                benefitInfo.InPlanNetwork = benefitInfo.InPlanNetwork;
                benefitInfo.ServiceTypes = benefitInfo.ServiceTypes;
                benefitInfo.InsuranceType = benefitInfo.InsuranceType;

                //Codes to ignore
                if (benefitInfo.InsuranceType.Code == "47" || benefitInfo.InsuranceType.Code == "12")
                    goto end;

                //Help find more codes and what they mean
                if (benefitInfo.InsuranceType.Code != "43" && benefitInfo.InsuranceType.Code != "MA" && benefitInfo.InsuranceType.Code != "MB" && benefitInfo.InsuranceType.Code != "OT" && benefitInfo.InsuranceType.Code != "14" && benefitInfo.InsuranceType.Code != "15")
                    MessageBox.Show(benefitInfo.InsuranceType.Code + " : " + benefitInfo.InsuranceType.Description);

                if (benefitInfo.InfoType.Code == "V")
                {
                    Back_End_Billing_Edit.Initializations.PublicVars.isEligible = false;
                    Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "Medicare cannot process";
                    return Tuple.Create(Back_End_Billing_Edit.Initializations.PublicVars.isEligible, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);
                }

                if (benefitInfo.InfoType.Code == "6" && benefitInfo.InsuranceType.Code == "MB")
                {
                    Back_End_Billing_Edit.Initializations.PublicVars.isEligible = true;
                    Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "Inactive Policy";
                    return Tuple.Create(Back_End_Billing_Edit.Initializations.PublicVars.isEligible, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);
                }

                //If HMO - Health Medicare Risk, Medicare Secondary Disabled Beneficiary, or Preferred Provider Organiztion (PPO)...
                if (benefitInfo.InsuranceType.Code == "HN" || benefitInfo.InsuranceType.Code == "12" || benefitInfo.InsuranceType.Code == "PR")
                {
                     //At one point, isEligible was made false... ?  commenting here to keep track of where that was
                     //Back_End_Billing_Edit.Initializations.PublicVars.isEligible = false;                    
                    try
                    {
                        // MADV + benefitInfo.Identifications[0].Id -- how to differentiate which insurance is being returned... will need a dictionary with payor keys and these IDs..
                        Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "MADV: " + benefitInfo.Identifications[0].Id + " " + benefitInfo.RelatedEntities[0].Name.LastName;
                        Back_End_Billing_Edit.Initializations.PublicVars.isEligible = true;
                        return Tuple.Create(Back_End_Billing_Edit.Initializations.PublicVars.isEligible, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);

                    }
                    catch
                    {
                        Back_End_Billing_Edit.Initializations.PublicVars.elMessage = "MADV ";
                        Back_End_Billing_Edit.Initializations.PublicVars.isEligible = true;
                        return Tuple.Create(Back_End_Billing_Edit.Initializations.PublicVars.isEligible, Back_End_Billing_Edit.Initializations.PublicVars.elMessage);
                    }                    
                }

                //benefitCodes += benefitInfo.InsuranceType.Code + " : ";
               
                
            }

            //MessageBox.Show(benefitCodes);
        end:
            return Tuple.Create(false, "Patient is Fine");
            #endregion
        }
    }
}
